# Installation Projet
Clonez le projet:
* git@gitlab.com:AlexPeg/mon-plus-beau-portfolio.git
* cd mon-plus-beau-portfolio.git

# Accédez à la version en ligne
Allez à l'adresse suivante 
* http://ultra-temper.surge.sh/

# Eco-index
* http://www.ecoindex.fr/resultats/?id=115243
* Résultats:Eco-index B

# Performance Score
 
 * https://developers.google.com/speed/pagespeed/insights/?hl=fr&url=http%3A%2F%2Fultra-temper.surge.sh%2F


